package com.pentilku.kalkulator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btn_1, btn_2, btn_3, btn_4, btn_5, btn_6, btn_7, btn_8, btn_9, btn_0, btn_bagi, btn_kali, btn_kurang, btn_sama, btn_clear, btn_tambah;
    private TextView tv_hasil, tv_status;
    private double val1 = 0;
    private double val2 = 0;
    private double val3;
    int akhir = 0;
    char action;
    char operator = 0;
    char flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setbtn();
        settex();

        btn_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals(" ")|| tv_status.getText().toString().equals("0")) {
                        tv_status.setText("0");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "0");
                    }
                }
                else {
                    cek();
                    if (tv_status.getText().toString().equals("0")||tv_status.getText().toString().equals(" "))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("0");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "0");
                    }

                }
            }
        });
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_hasil.setText("0");
                tv_status.setText(" ");
                val1 = 0;
                val2 = 0;
                val3 = 0;
                operator = 0;
                flag = 0;
            }
        });
        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("1");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "1");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("1");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "1");
                    }

                }
            }
        });
        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("2");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "2");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("2");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "2");
                    }

                }
            }
        });
        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("3");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "3");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("3");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "3");
                    }

                }
            }
        });
        btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("4");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "4");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("4");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "4");
                    }
                }
            }
        });
        btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("5");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "5");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("5");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "5");
                    }

                }
            }
        });
        btn_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("6");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "6");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("6");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "6");
                    }

                }
            }
        });
        btn_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("7");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "7");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("7");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "7");
                    }

                }
            }
        });
        btn_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("8");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "8");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("8");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "8");
                    }

                }
            }
        });
        btn_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (akhir == 0) {
                    if (tv_status.getText().toString().equals("0")) {
                        tv_status.setText("9");
                    } else {
                        tv_status.setText(tv_status.getText().toString() + "9");
                    }
                } else {
                    cek();
                    if (tv_status.getText().toString().equals("0"))
                    {
                        tv_hasil.setText("0");
                        tv_status.setText("9");
                    }
                    else
                    {
                        tv_status.setText(tv_status.getText().toString() + "9");
                    }

                }
            }
        });

        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                action = '+';
                nilai1(action);
                flag = 1;
            }
        });

        btn_kali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                action = '*';
                nilai1(action);
                flag = 1;
            }
        });

        btn_kurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                action = '-';
                nilai1(action);
                flag = 1;
            }
        });

        btn_bagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                action = '/';
                nilai1(action);
                flag = 1;
            }
        });

        btn_sama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fungsiSamaDengan();
            }
        });
    }

    private void fungsiSamaDengan() {
        if (akhir == 0 && !tv_status.getText().toString().equals(" ")) {
            val2 = Double.parseDouble(tv_status.getText().toString());
        }

        if (action == ('+')) {
            val1 = val1 + val2;
        } else if (action == '-') {
            val1 = val1 - val2;
        } else if (action == ('*')) {
            val1 = val1 * val2;
        } else if (action == ('/')) {
            val1 = val1 / val2;
        }
        tv_hasil.setText(String.valueOf(val1));
        tv_status.setText("0");
        akhir = 1;
//        if (val2 != 0) {
//            flag = 0;
//            Log.d("masfan", "val != 0");
//        } else {
//            Log.d("masfan", "val == 0");
//        }
    }

    private void setbtn() {
        btn_1 = (Button) findViewById(R.id.btn_1);
        btn_0 = (Button) findViewById(R.id.btn_0);
        btn_2 = (Button) findViewById(R.id.btn_2);
        btn_3 = (Button) findViewById(R.id.btn_3);
        btn_4 = (Button) findViewById(R.id.btn_4);
        btn_5 = (Button) findViewById(R.id.btn_5);
        btn_6 = (Button) findViewById(R.id.btn_6);
        btn_7 = (Button) findViewById(R.id.btn_7);
        btn_8 = (Button) findViewById(R.id.btn_8);
        btn_9 = (Button) findViewById(R.id.btn_9);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_bagi = (Button) findViewById(R.id.btn_bagi);
        btn_kali = (Button) findViewById(R.id.btn_kali);
        btn_tambah = (Button) findViewById(R.id.btn_tambah);
        btn_kurang = (Button) findViewById(R.id.btn_kurang);
        btn_sama = (Button) findViewById(R.id.btn_sama);
    }

    private void settex() {
        tv_hasil = (TextView) findViewById(R.id.tv_hasil);
        tv_status = (TextView) findViewById(R.id.tv_status);
    }

    private void nilai1(char action) {
        if (flag == 1) { // isi val2
            operator = action;

            if (operator == '/') {
                if (akhir == 0) {
                    val2 = Double.parseDouble(tv_status.getText().toString());
                    val1 = val1 / val2;
                }
            } else if (operator == '*') {
                if (akhir == 0) {
                    val2 = Double.parseDouble(tv_status.getText().toString());
                    val1 = val1 * val2;
                }
            } else if (operator == '+') {
                if (akhir == 0) {
                    val2 = Double.parseDouble(tv_status.getText().toString());
                    val1 = val1 + val2;
                }
            } else if (operator == '-') {
                if (akhir == 0) {
                    val2 = Double.parseDouble(tv_status.getText().toString());
                    val1 = val1 - val2;
                }
            }
            tv_hasil.setText(String.valueOf(val1) + operator);
            tv_status.setText("0");
        } else { // isi val 1
            if (tv_status.getText().toString().equals(" ")) {
                val1 = 0;
                tv_hasil.setText(String.valueOf(val1) + operator);
            } else {
                if (action == '+') {
                    tv_hasil.setText(tv_status.getText().toString() + " +");
                    operator = '+';
                } else if (action == '-') {
                    tv_hasil.setText(tv_status.getText().toString() + " -");
                    operator = '-';
                } else if (action == '*') {
                    tv_hasil.setText(tv_status.getText().toString() + " *");
                    operator = '*';
                } else if (action == '/') {
                    tv_hasil.setText(tv_status.getText().toString() + " /");
                    operator = '/';
                }
                val1 = Double.parseDouble(tv_status.getText().toString());
                tv_status.setText("0");
            }
        }
        akhir = 0;
    }

    public void cek ()
    {
        val1 = 0;
        val2 = 0;
        val3 = 0;
        operator = 0;
        flag = 0;
    }
}
